import json

import schema
from django import http
from django import views
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core import exceptions
from django.core import paginator as pagination
from schema import And
from schema import Optional
from schema import Or
from schema import Schema
from schema import Use

from photobank.util.pagination import URLPaginator
from photobank.util.pagination import get_paged_response


class UsersView(views.View):
    def get(self, request):
        users = get_user_model().objects.all()
        paginator = URLPaginator(users, request.pagesize)
        try:
            users = paginator.page(request.page)
        except pagination.InvalidPage:
            users = pagination.Page([], 1, paginator)
        response = json.dumps(get_paged_response(users, paginator, request)).encode()
        return http.HttpResponse(response, status=200)

    def post(self, request):
        data = json.loads(request.body.decode())
        try:
            data = self._validate_create_data(data)
        except schema.SchemaError as e:
            response = json.dumps({'message': str(e)}).encode()
            return http.HttpResponse(response, status=400)
        user = get_user_model()(**data)
        try:
            user.validate_unique()
        except exceptions.ValidationError as e:
            response = json.dumps({'message': str(e)}).encode()
            return http.HttpResponse(response, status=409)
        user.save()
        response = json.dumps(user.as_dict()).encode()
        return http.HttpResponse(response, status=200)

    @classmethod
    def _validate_create_data(cls, data):
        validator = Schema({
            Optional('password'): And(And(str, len), Use(make_password)),
            Optional('username'): And(str, len),
            Optional('first_name'): Or(str, None),
            Optional('last_name'): Or(str, None),
            Optional('email'): Or(str, None),
        })
        return validator.validate(data)


class UsersAuthView(views.View):
    def post(self, request):
        data = json.loads(request.body.decode())
        try:
            data = self._validate_auth_data(data)
        except schema.SchemaError as e:
            response = json.dumps({'message': str(e)}).encode()
            return http.HttpResponse(response, status=400)
        user = authenticate(**data)
        if not user:
            response = json.dumps({'message': 'Unauthorized'}).encode()
            return http.HttpResponse(response, status=401)
        response = json.dumps({'token': user.jwt, 'user': user.as_dict()}).encode()
        return http.HttpResponse(response, status=200)

    @classmethod
    def _validate_auth_data(cls, data):
        validator = Schema({
            'password': And(str, len),
            'username': And(str, len),
        })
        return validator.validate(data)


class UserView(views.View):
    def get(self, request, user_id):
        try:
            user = get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            response = json.dumps({'message': 'Not Found'}).encode()
            return http.HttpResponse(response, status=404)
        response = json.dumps(user.as_dict()).encode()
        return http.HttpResponse(response, status=200)

    def patch(self, request, user_id):
        user_id = int(user_id)
        request_user = authenticate(token=request.token)
        if not request_user:
            response = json.dumps({'message': 'Unauthorized'}).encode()
            return http.HttpResponse(response, status=401)
        if not (request_user.is_staff or request_user.id == user_id):
            response = json.dumps({'message': 'Forbidden'}).encode()
            return http.HttpResponse(response, status=403)

        try:
            user = get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            response = json.dumps({'message': 'Not Found'}).encode()
            return http.HttpResponse(response, status=404)
        data = json.loads(request.body.decode())
        try:
            data = self._validate_update_data(data)
        except schema.SchemaError as e:
            response = json.dumps({'message': str(e)}).encode()
            return http.HttpResponse(response, status=400)
        if data:
            for key, value in data.items():
                setattr(user, key, value)
            user.save()
        response = json.dumps(user.as_dict()).encode()
        return http.HttpResponse(response, status=200)

    @classmethod
    def _validate_update_data(cls, data):
        validator = Schema({
            Optional('password'): And(And(str, len), Use(make_password)),
            Optional('first_name'): Or(str, None),
            Optional('last_name'): Or(str, None),
        })
        return validator.validate(data)
