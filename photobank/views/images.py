import datetime
import json

import schema
from django import http
from django import views
from django.contrib.auth import get_user_model
from django.core import paginator as pagination
from rest_framework.compat import authenticate
from schema import And
from schema import Optional
from schema import Regex
from schema import Schema
from schema import Use

from photobank.models import Image
from photobank.util.pagination import URLPaginator
from photobank.util.pagination import get_paged_response


class ImagesView(views.View):
    def get(self, request):
        images = Image.objects.all()
        owner_username = request.GET.get('owner_username')
        owner_id = request.GET.get('owner_id')
        date = request.GET.get('date')
        name = request.GET.get('name')
        description = request.GET.get('description')
        if owner_username is not None:
            images = images.filter(owner__username=owner_username)
        if owner_id is not None:
            images = images.filter(owner__id=owner_id)
        if date is not None:
            images = images.filter(date__startswith=date)
        if name is not None:
            images = images.filter(name__icontains=name)
        if description is not None:
            images = images.filter(description__icontains=description)
        paginator = URLPaginator(images, request.pagesize)
        try:
            images = paginator.page(request.page)
        except pagination.InvalidPage:
            images = pagination.Page([], 1, paginator)
        response = json.dumps(get_paged_response(images, paginator, request)).encode()
        return http.HttpResponse(response, status=200)

    def post(self, request):
        request_user = authenticate(token=request.token)
        if not request_user:
            response = json.dumps({'message': 'Unauthorized'}).encode()
            return http.HttpResponse(response, status=401)
        data = request.POST.dict()
        data['image'] = request.FILES.get('image')
        data['owner'] = request_user
        try:
            data = self._validate_create_data(data)
        except schema.SchemaError as e:
            response = json.dumps({'message': str(e)}).encode()
            return http.HttpResponse(response, status=400)
        image = Image(**data)
        image.save()
        response = json.dumps(image.as_dict(request=request)).encode()
        return http.HttpResponse(response, status=200)

    @classmethod
    def _validate_create_data(cls, data):
        validator = Schema({
            'image': lambda i: i is not None,
            'owner': get_user_model(),
            'name': And(str, len),
            'description': str,
            'date': And(And(Regex(r'^\d{4}-\d{2}-\d{2}$'),
                            Use(lambda string: datetime.datetime.strptime(string, '%Y-%m-%d').date())),
                        lambda date: date <= (datetime.datetime.utcnow() + datetime.timedelta(hours=12)).date()),
        })
        return validator.validate(data)


class ImageView(views.View):
    def get(self, request, image_id):
        try:
            image = Image.objects.get(pk=image_id)
        except Image.DoesNotExist:
            response = json.dumps({'message': 'Not Found'}).encode()
            return http.HttpResponse(response, status=404)
        response = json.dumps(image.as_dict(request=request)).encode()
        return http.HttpResponse(response, status=200)

    def patch(self, request, image_id):
        try:
            image = Image.objects.get(pk=image_id)
        except Image.DoesNotExist:
            response = json.dumps({'message': 'Not Found'}).encode()
            return http.HttpResponse(response, status=404)
        ownership_response = self._check_ownership(request, image)
        if ownership_response.status_code != 200:
            return ownership_response
        data = json.loads(request.body.decode())
        try:
            data = self._validate_update_data(data)
        except schema.SchemaError as e:
            response = json.dumps({'message': str(e)}).encode()
            return http.HttpResponse(response, status=400)
        if data:
            for key, value in data.items():
                setattr(image, key, value)
            image.save()
        response = json.dumps(image.as_dict(request=request)).encode()
        return http.HttpResponse(response, status=200)

    def delete(self, request, image_id):
        try:
            image = Image.objects.get(pk=image_id)
        except Image.DoesNotExist:
            response = json.dumps({'message': 'Not Found'}).encode()
            return http.HttpResponse(response, status=404)
        ownership_response = self._check_ownership(request, image)
        if ownership_response.status_code != 200:
            return ownership_response
        image.delete()
        return http.HttpResponse('OK', status=200)

    @classmethod
    def _validate_update_data(cls, data):
        validator = Schema({
            Optional('name'): And(str, len),
            Optional('description'): str,
            Optional('date'): And(And(Regex(r'^\d{4}-\d{2}-\d{2}$'),
                                      Use(lambda string: datetime.datetime.strptime(string, '%Y-%m-%d').date())),
                                  lambda date: date <= (datetime.datetime.utcnow() + datetime.timedelta(hours=12)).date()),
        })
        return validator.validate(data)

    @classmethod
    def _check_ownership(cls, request, entity):
        request_user = authenticate(token=request.token)
        if not request_user:
            response = json.dumps({'message': 'Unauthorized'}).encode()
            return http.HttpResponse(response, status=401)
        if not (request_user.is_staff or request_user == entity.owner):
            response = json.dumps({'message': 'Forbidden'}).encode()
            return http.HttpResponse(response, status=403)
        return http.HttpResponse(status=200)
