from django.conf.urls import url

from photobank.views import ImageView
from photobank.views import ImagesView
from photobank.views import UserView
from photobank.views import UsersAuthView
from photobank.views import UsersView

urlpatterns = [
    url(r'^users/(?P<user_id>\d+)/', UserView.as_view()),
    url(r'^users/auth/', UsersAuthView.as_view()),
    url(r'^users/', UsersView.as_view()),
    url(r'^images/(?P<image_id>\d+)/', ImageView.as_view()),
    url(r'^images/', ImagesView.as_view()),
]
