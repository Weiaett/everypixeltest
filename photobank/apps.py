from django.apps import AppConfig


class PhotobankConfig(AppConfig):
    name = 'photobank'
