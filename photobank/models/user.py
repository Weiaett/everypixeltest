from django.contrib.auth.models import AbstractUser

from photobank.util.auth import JWT


class User(AbstractUser):

    @property
    def jwt(self):
        return JWT(uid=self.id).encode()

    def as_dict(self, **kwargs):
        return {
            'id': self.id,
            'username': self.username,
            'is_staff': self.is_staff,
            'first_name': self.first_name,
            'last_name': self.last_name,
        }
