import datetime
import os

from django.conf import settings
from django.db import models
from django.dispatch import receiver


def user_directory_path(instance, filename):
    root, extension = os.path.splitext(filename)
    return f'{instance.owner.id}/{int(datetime.datetime.utcnow().timestamp() * 1000)}-{instance.name}{extension}'


class Image(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='images')
    image = models.ImageField(upload_to=user_directory_path)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=255, blank=True)
    date = models.DateField()

    class Meta:
        ordering = ('-id',)

    def as_dict(self, **kwargs):
        return {
            'id': self.id,
            'owner': self.owner.as_dict(),
            'image': kwargs.get('request') and kwargs.get('request').build_absolute_uri(self.image.url) or self.image.url,
            'name': self.name,
            'description': self.description,
            'date': self.date.isoformat(),
        }


@receiver(models.signals.post_delete, sender=Image)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)
