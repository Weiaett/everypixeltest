from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from photobank.models import Image
from photobank.models import User

admin.site.register(Image)
admin.site.register(User, UserAdmin)
