class Middleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        header = request.META.get('HTTP_AUTHORIZATION')
        token = header.split()[-1] if header and header.startswith('Bearer') else None
        request.token = token
        request.page = request.GET.get('page', 1)
        request.pagesize = request.GET.get('pagesize', 25)
        response = self.get_response(request)
        if request.path.startswith('/api/'):
            response['Content-Type'] = 'application/json'
        return response
