import collections
import urllib

from django.core import paginator as pagination

URLPages = collections.namedtuple('URLPages', ('prev', 'next'))


class URLPaginator(pagination.Paginator):
    def url_pages(self, url, page):
        prev_page = None
        next_page = None
        parsed_url = urllib.parse.urlparse(url)
        parsed_query = urllib.parse.parse_qs(parsed_url.query)
        if page.has_previous():
            parsed_query['page'] = page.previous_page_number()
            prev_page = urllib.parse.urlunparse((
                parsed_url[0],
                parsed_url[1],
                parsed_url[2],
                parsed_url[3],
                urllib.parse.urlencode(parsed_query, doseq=True),
                parsed_url[5],
            ))
        if page.has_next():
            parsed_query['page'] = page.next_page_number()
            next_page = urllib.parse.urlunparse((
                parsed_url[0],
                parsed_url[1],
                parsed_url[2],
                parsed_url[3],
                urllib.parse.urlencode(parsed_query, doseq=True),
                parsed_url[5],
            ))
        return URLPages(prev_page, next_page)


def get_paged_response(data, paginator, request):
    pages = paginator.url_pages(request.build_absolute_uri(), data)
    return {
        'count': paginator.count,
        'pages': paginator.num_pages,
        'next': pages.next,
        'previous': pages.prev,
        'results': [item.as_dict(request=request) for item in data],
    }
