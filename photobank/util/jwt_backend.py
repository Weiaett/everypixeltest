from django.contrib.auth import get_user_model

from photobank.util.auth import JWT


class JWTBackend:
    def authenticate(self, token):
        jwt = token and JWT.decode(token)
        if not jwt:
            return None
        try:
            user = get_user_model().objects.get(pk=jwt.uid)
            return user
        except get_user_model().DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None
