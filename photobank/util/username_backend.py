from django.contrib.auth import get_user_model


class UsernameBackend:
    def authenticate(self, username, password):
        try:
            user = get_user_model().objects.get(username=username)
            return user if user.check_password(password) else None
        except get_user_model().DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None
