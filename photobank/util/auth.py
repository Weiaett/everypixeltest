import datetime

import jwt

from everypixel import config


class JWT:
    def __init__(self, uid, exp=None):
        self.uid = uid
        self.exp = exp or datetime.datetime.utcnow() + datetime.timedelta(days=config.JWT_EXP)

    def encode(self):
        token = {
            'uid': self.uid,
            'exp': self.exp,
        }
        token = jwt.encode(token, key=config.JWT_KEY)
        token = token.decode()
        return token

    @classmethod
    def decode(cls, token):
        try:
            token = jwt.decode(token, key=config.JWT_KEY, algorithms=['HS256'])
            uid = token.get('uid')
            exp = token.get('exp')
            return cls(uid, exp)
        except jwt.DecodeError:
            return None
        except jwt.ExpiredSignatureError:
            return None
