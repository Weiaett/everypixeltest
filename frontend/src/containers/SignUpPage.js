import { connect } from 'react-redux';
import { signUp, resetSignUpState } from '../actions/auth';
import SignUpForm from '../components/SignUpForm/SignUpForm';

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    isSignUpRequest: state.auth.isSignUpRequest,
    isSignUpSuccess: state.auth.isSignUpSuccess,
    isSignUpFailure: state.auth.isSignUpFailure,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signUp: (username, password) => {
      dispatch(signUp(username, password));
    },
    resetState: () => {
      dispatch(resetSignUpState())
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);
