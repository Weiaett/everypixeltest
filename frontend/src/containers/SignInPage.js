import { connect } from 'react-redux';
import { signIn, resetSignInState } from '../actions/auth';
import SignInForm from '../components/SignInForm/SignInForm';

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    isSignInRequest: state.auth.isSignInRequest,
    isSignInSuccess: state.auth.isSignInSuccess,
    isSignInFailure: state.auth.isSignInFailure,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signIn: (username, password) => {
      dispatch(signIn(username, password));
    },
    resetState: () => {
      dispatch(resetSignInState())
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm);
