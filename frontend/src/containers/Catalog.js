import { connect } from 'react-redux';
import { getImages, deleteImage, updateImage, createImage } from '../actions/catalog';
import Catalog from '../components/Catalog/Catalog';

function mapStateToProps(state) {
  return {
    images: state.catalog.images,
    pageNowAt: state.catalog.pageNowAt,
    pageCount: state.catalog.pageCount,
    user: state.auth.user,
    token: state.auth.token,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getImages: (query, page) => {
      dispatch(getImages(query, page));
    },
    deleteImage: (imageId, token) => {
      dispatch(deleteImage(imageId, token));
    },
    updateImage: (imageId, token, data) => {
      dispatch(updateImage(imageId, token, data));
    },
    createImage: (token, data) => {
      dispatch(createImage(token, data));
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
