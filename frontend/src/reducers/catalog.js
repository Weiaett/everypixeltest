import {
  GET_IMAGES_REQUEST,
  GET_IMAGES_SUCCESS,
  GET_IMAGES_FAILURE,
  PAGE_SIZE,
  DELETE_IMAGE_REQUEST,
  DELETE_IMAGE_SUCCESS,
  DELETE_IMAGE_FAILURE,
  UPDATE_IMAGE_REQUEST,
  UPDATE_IMAGE_SUCCESS,
  UPDATE_IMAGE_FAILURE,
  CREATE_IMAGE_REQUEST,
  CREATE_IMAGE_SUCCESS,
  CREATE_IMAGE_FAILURE,
} from '../actions/catalog';

const initialState = {
  images: [],
  pageNowAt: 1,
  pageCount: 1,
  isGetImagesRequest: false,
  isGetImagesSuccess: false,
  isGetImagesFailure: false,
  isDeleteImageRequest: false,
  isDeleteImageSuccess: false,
  isDeleteImageFailure: false,
  isUpdateImageRequest: false,
  isUpdateImageSuccess: false,
  isUpdateImageFailure: false,
  isAddImageRequest: false,
  isAddImageSuccess: false,
  isAddImageFailure: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_IMAGES_REQUEST:
      return {
        ...state,
        isGetImagesRequest: true,
        isGetImagesSuccess: false,
        isGetImagesFailure: false,
      };
    case GET_IMAGES_SUCCESS:
      return {
        ...state,
        images: action.payload.results,
        pageNowAt: action.payload.page,
        pageCount: Math.ceil(action.payload.count / PAGE_SIZE),
        isGetImagesSuccess: true,
        isGetImagesRequest: false,
        isGetImagesFailure: false,
      };
    case GET_IMAGES_FAILURE:
      return {
        ...state,
        isGetImagesFailure: true,
        isGetImagesRequest: false,
        isGetImagesSuccess: false,
      };
    case DELETE_IMAGE_REQUEST:
      return {
        ...state,
        isDeleteImageRequest: true,
        isDeleteImageSuccess: false,
        isDeleteImageFailure: false,
      };
    case DELETE_IMAGE_SUCCESS:
      return {
        ...state,
        images: state.images.filter(image => image.id !== action.payload.imageId),
        isDeleteImageSuccess: true,
        isDeleteImageRequest: false,
        isDeleteImageFailure: false,
      };
    case DELETE_IMAGE_FAILURE:
      return {
        ...state,
        isDeleteImageFailure: true,
        isDeleteImageRequest: false,
        isDeleteImageSuccess: false,
      };
    case UPDATE_IMAGE_REQUEST:
      return {
        ...state,
        isUpdateImageRequest: true,
        isUpdateImageSuccess: false,
        isUpdateImageFailure: false,
      };
    case UPDATE_IMAGE_SUCCESS:
      return {
        ...state,
        images: state.images.map(image => image.id === action.payload.id ? action.payload : image),
        isUpdateImageSuccess: true,
        isUpdateImageRequest: false,
        isUpdateImageFailure: false,
      };
    case UPDATE_IMAGE_FAILURE:
      return {
        ...state,
        isUpdateImageFailure: true,
        isUpdateImageRequest: false,
        isUpdateImageSuccess: false,
      };

    case CREATE_IMAGE_REQUEST:
      return {
        ...state,
        isUpdateImageRequest: true,
        isUpdateImageSuccess: false,
        isUpdateImageFailure: false,
      };
    case CREATE_IMAGE_SUCCESS:
      return {
        ...state,
        images: [action.payload, ...state.images],
        isCreateImageSuccess: true,
        isCreateImageRequest: false,
        isCreateImageFailure: false,
      };
    case CREATE_IMAGE_FAILURE:
      return {
        ...state,
        isCreateImageFailure: true,
        isCreateImageRequest: false,
        isCreateImageSuccess: false,
      };
    default:
      return state;
  }
};
