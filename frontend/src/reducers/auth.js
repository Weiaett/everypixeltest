import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_OUT,
  SIGN_IN_RESET_STATE,
  SIGN_UP_RESET_STATE,
} from '../actions/auth';

const initialState = {
  token: null,
  user_id: null,
  isAuthenticated: false,
  isSignInRequest: false,
  isSignInSuccess: false,
  isSignInFailure: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN_REQUEST:
      return {
        ...state,
        isSignInRequest: true,
        isSignInSuccess: false,
        isSignInFailure: false,
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        token: action.payload.token,
        user: action.payload.user,
        isAuthenticated: true,
        isSignInSuccess: true,
        isSignInRequest: false,
        isSignInFailure: false,
      };
    case SIGN_IN_FAILURE:
      return {
        ...state,
        isSignInFailure: true,
        isSignInRequest: false,
        isSignInSuccess: false,
      };
    case SIGN_UP_REQUEST:
      return {
        ...state,
        isSignUpRequest: true,
        isSignUpSuccess: false,
        isSignUpFailure: false,
      };
    case SIGN_UP_SUCCESS:
      return {
        ...state,
        isSignUpSuccess: true,
        isSignUpRequest: false,
        isSignUpFailure: false,
      };
    case SIGN_UP_FAILURE:
      return {
        ...state,
        isSignUpFailure: true,
        isSignUpRequest: false,
        isSignUpSuccess: false,
      };
    case SIGN_OUT:
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        isSignUpRequest: false,
        isSignUpSuccess: false,
        isSignUpFailure: false,
      };
    case SIGN_IN_RESET_STATE:
      return {
        ...state,
        isSignInRequest: false,
        isSignInSuccess: false,
        isSignInFailure: false,
      };
    case SIGN_UP_RESET_STATE:
      return {
        ...state,
        isSignUpRequest: false,
        isSignUpSuccess: false,
        isSignUpFailure: false,
      };
    default:
      return state;
  }
};
