import { combineReducers } from 'redux';
import auth from './auth';
import catalog from './catalog';

const rootReducer = combineReducers({
  auth,
  catalog,
});

export default rootReducer;
