import './Catalog.css';
import React, { Component, Fragment } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardText,
  CardTitle,
  CardSubtitle,
  Container,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
} from 'reactstrap';

const ImageBody = props => (
  <div style={{
    cursor: 'pointer',
    width: '100%',
    height: '14rem',
    backgroundImage: `url('${props.src}')`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  }} {...props}/>
);

class ImageModal extends Component {
  constructor(props) {
    super(props);
    const { name = 'Название', description = '', date = '1970-01-01'} = this.props.image || {};
    this.state = {
      name,
      description,
      date,
      image: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    const value = name === 'image' ? event.target.files[0] : event.target.value;

    this.setState(state => ({
      ...state,
      [name]: value,
    }));
  }

  handleSubmit() {
    if (this.props.isEdit) {
      const { updateImage, image, token } = this.props;
      updateImage(image.id, token, { ...this.state, image: undefined });
    } else {
      const { createImage, token } = this.props;
      createImage(token, this.state);
    }
  }

  handleDelete() {
    const { deleteImage, image, token } = this.props;
    deleteImage(image.id, token);
  }

  render() {
    const { isOpen, toggle, isEdit, isEditable } = this.props;
    return (
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Edit image info</ModalHeader>
        <ModalBody>
          <Form>
            {!isEdit && <FormGroup>
              <Label for="image">File</Label>
              <Input type="file" name="image" onChange={this.handleChange}/>
            </FormGroup>}
            <FormGroup>
              <Label for="name">Name</Label>
              <Input type="text" name="name" value={this.state.name} disabled={!isEditable} onChange={this.handleChange}/>
            </FormGroup>
            <FormGroup>
              <Label for="description">Description</Label>
              <Input type="textarea" name="description" value={this.state.description} disabled={!isEditable} onChange={this.handleChange}/>
            </FormGroup>
            <FormGroup>
              <Label for="date">Date</Label>
              <Input type="date" name="date" value={this.state.date} disabled={!isEditable} onChange={this.handleChange}/>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button type="button" color="primary" onClick={this.handleSubmit}>Submit</Button>
          {isEdit && <Button type="button" color="danger" disabled={!isEditable} onClick={this.handleDelete}>Delete</Button>}
          <Button type="button" color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const ImageOverlay = props => {
  const { image: { image, name }, isOpen, toggle } = props;
  return isOpen && (
    <div className="image-overlay" onClick={toggle}>
      <img src={image} alt={name} className="img-fluid"/>
    </div>
  );
};

class ImageCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      isOverlayOpen: false,
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleOverlay = this.toggleOverlay.bind(this);
  }

  toggleModal() {
    this.setState(state => ({
      isModalOpen: !state.isModalOpen,
    }));
  }

  toggleOverlay() {
    this.setState(state => ({
      isOverlayOpen: !state.isOverlayOpen,
    }));
  }

  render() {
    const { owner, image, date, name } = this.props.image;
    const { user, token, deleteImage, updateImage } = this.props;
    const isOwnedByUser = user !== undefined && user.id === owner.id;
    return (
      <Fragment>
        <Card outline={isOwnedByUser} color={isOwnedByUser ? "success" : ""}>
          <CardBody>
            <span className="image-card-edit text-muted" onClick={this.toggleModal}>
              <i className="far fa-edit"/>
            </span>
            <CardTitle>{name}</CardTitle>
            <CardSubtitle className="text-muted">by <b>{owner.username}</b></CardSubtitle>
            <CardText>
              <small className="text-muted">captured: {date}</small>
            </CardText>
          </CardBody>
          <ImageBody src={image} onClick={this.toggleOverlay}/>
        </Card>
        <ImageModal image={this.props.image}
                    isOpen={this.state.isModalOpen}
                    toggle={this.toggleModal}
                    isEdit={true}
                    isEditable={user && (user.is_staff || isOwnedByUser)}
                    token={token}
                    deleteImage={deleteImage}
                    updateImage={updateImage}/>
        <ImageOverlay image={this.props.image} isOpen={this.state.isOverlayOpen} toggle={this.toggleOverlay}/>
      </Fragment>
    );
  }
}

const FloatingButton = props => {

  return (
    <Button
      size="lg"
      color="primary"
      className="floating-button" {...props}>
      {props.children}
    </Button>
  );
};

class Catalog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      query: '',
      isQueryValid: null,
      isModalOpen: false,
    };
    this.onChange = this.onChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal() {
    this.setState(state => ({
      isModalOpen: !state.isModalOpen,
    }));
  }


  static parseQueryString(value) {
    const clauses = value.trim().split(',').map(c => c.trim().split(':').map(i => i.trim()));
    const queries = clauses.reduce((obj, [k, v]) => ({ ...obj, [k]: v }), {});
    let isValid = true;
    for (const key in queries) {
      if (!['date', 'name', 'description', 'owner'].includes(key) || !queries[key]) {
        isValid = false;
        break;
      }
    }
    if (queries.hasOwnProperty('owner')) {
      queries.owner_username = queries.owner;
      delete queries.owner;
    }
    return isValid ? queries : '';
  }

  onChange(event) {
    const value = event.target.value;
    const query = Catalog.parseQueryString(value);
    const isQueryValid = value === '' ? null : !!query;
    this.setState({
      value,
      query,
      isQueryValid,
    });
  }

  getImages(page = 1) {
    const { getImages } = this.props;
    if (this.state.isQueryValid || this.state.query === '') {
      getImages(this.state.query, page);
    }
  }

  onSearch() {
    this.getImages();
  }

  componentDidMount() {
    this.getImages();
  }

  render() {
    const { images, pageNowAt, pageCount, user, token, deleteImage, updateImage, createImage } = this.props;
    const onFirstPage = pageNowAt === 1;
    const onLastPage = pageNowAt === pageCount;

    return (
      <Container style={{ marginTop: '1.5rem' }}>
        <InputGroup>
          <Input placeholder="e.g. name: image, owner: username"
                 valid={this.state.isQueryValid === true}
                 invalid={this.state.isQueryValid === false}
                 value={this.state.value}
                 onChange={this.onChange}/>
          <InputGroupAddon addonType="append">
            <Button color="primary" onClick={this.onSearch}>Search</Button>
          </InputGroupAddon>
        </InputGroup>
        <div className="catalog-pagination">
          <Pagination>
            <PaginationItem>
              <PaginationLink previous disabled={onFirstPage} onClick={() => this.getImages(pageNowAt - 1)}/>
            </PaginationItem>
            {
              [...Array(pageCount).keys()].map(i => {
                const pageIndex = i + 1;
                return (
                  <PaginationItem key={pageIndex} active={pageIndex === pageNowAt}>
                    <PaginationLink href="#" onClick={() => this.getImages(pageIndex)}>{pageIndex}</PaginationLink>
                  </PaginationItem>
                );
              })
            }
            <PaginationItem>
              <PaginationLink next disabled={onLastPage} onClick={() => this.getImages(pageNowAt + 1)}/>
            </PaginationItem>
          </Pagination>
        </div>
        <div className="catalog">
          {
            images.map(image => <ImageCard key={image.id}
                                           image={image}
                                           user={user}
                                           token={token}
                                           deleteImage={deleteImage}
                                           updateImage={updateImage}/>)
          }
        </div>
        {user && <FloatingButton onClick={this.toggleModal}>
          <i className="fas fa-plus"/>
        </FloatingButton>}
        <ImageModal isOpen={this.state.isModalOpen}
                    toggle={this.toggleModal}
                    isEdit={false}
                    isEditable={true}
                    token={token}
                    createImage={createImage}/>
      </Container>
    );
  }
}

export default Catalog;
