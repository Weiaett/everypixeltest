import './SignInForm.css';
import React, { Component } from 'react';
import { Redirect } from 'react-router';
import {
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  UncontrolledAlert,
} from 'reactstrap';

class SignInForm extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      data: {
        username: {
          value: '',
          isValid: false,
          isValidated: false,
        },
        password: {
          value: '',
          isValid: false,
          isValidated: false,
        },
      },
      isFormValid: false,
    };
  }

  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      data: {
        ...this.state.data,
        [name]: {
          ...this.state.data[name],
          value,
        }
      }
    }, () => this.validateField(name, value));
  }

  handleSubmit(e) {
    const { signIn, isSignInRequest } = this.props;
    const username = this.state.data.username.value;
    const password = this.state.data.password.value;

    if (this.state.isFormValid && !isSignInRequest) {
      signIn(username, password);
    } else {
      this.setState({
        data: Object.keys(this.state.data).reduce((p, c) => ({...p, [c]: {...this.state.data[c], isValidated: true} }), {})
      });
    }

    e.preventDefault();
  }

  validateField(name, value) {
    let isValid = this.state.data[name].isValid;
    let isValidated = this.state.data[name].isValidated;

    switch (name) {
      case 'username': {
        isValid = Boolean(value);
        isValidated = true;
        break;
      }
      case 'password': {
        isValid = Boolean(value);
        isValidated = true;
        break;
      }
      default:
        break;
    }

    this.setState({
      data: {
        ...this.state.data,
        [name]: {
          ...this.state.data[name],
          isValid,
          isValidated,
        }
      }
    }, this.validateForm);
  }

  validateForm() {
    this.setState({
      isFormValid: Object.keys(this.state.data).reduce((p, c) => p && this.state.data[c].isValid, true)
    });
  }

  getFieldValidState(name) {
    return this.state.data[name].isValidated && this.state.data[name].isValid === true;
  }

  getFieldInvalidState(name) {
    return this.state.data[name].isValidated && this.state.data[name].isValid !== true;
  }

  render() {
    const {
      isAuthenticated,
      isSignInSuccess,
      isSignInFailure,
      resetState,
    } = this.props;

    const {
      data,
      isFormValid
    } = this.state;

    if (isAuthenticated) {
      resetState();
      return <Redirect to="/"/>
    }

    let result = '';
    if(isSignInSuccess) result = (
      <UncontrolledAlert color="success">
        Успешная авторизация
      </UncontrolledAlert>
    );
    if(isSignInFailure) result = (
      <UncontrolledAlert color="danger">
        Произошла ошибка
      </UncontrolledAlert>
    );

    return (
      <div className="sign-in-form-container">
        <Form className="sign-in-form">
          <h4 className="text-center">Авторизация</h4>
          {result}
          <FormGroup>
            <Label for="username">Login</Label>
            <Input type="text"
                   name="username"
                   valid={this.getFieldValidState('username')}
                   invalid={this.getFieldInvalidState('username')}
                   value={data.username.value}
                   onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="password">Пароль</Label>
            <Input type="password"
                   name="password"
                   valid={this.getFieldValidState('password')}
                   invalid={this.getFieldInvalidState('password')}
                   value={data.password.value}
                   onChange={this.handleChange}/>
          </FormGroup>
          <Button block
                  color="primary"
                  className="mt-4"
                  disabled={!isFormValid}
                  onClick={this.handleSubmit}>Авторизоваться</Button>
        </Form>
      </div>
    );
  }
}

export default SignInForm;
