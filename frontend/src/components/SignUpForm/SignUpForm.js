import './SignUpForm.css';
import React, { Component } from 'react';
import { Redirect } from 'react-router';
import {
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  UncontrolledAlert,
} from 'reactstrap';

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      data: {
        username: {
          value: '',
          isValid: false,
          isValidated: false,
        },
        password: {
          value: '',
          isValid: false,
          isValidated: false,
        },
        'password-confirm': {
          value: '',
          isValid: false,
          isValidated: false,
        },
      },
      isFormValid: false,
    };
  }

  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      data: {
        ...this.state.data,
        [name]: {
          ...this.state.data[name],
          value,
        }
      }
    }, () => this.validateField(name, value));
  }

  handleSubmit(e) {
    const { signUp, isSignUpRequest } = this.props;
    let username = this.state.data.username.value;
    let password = this.state.data.password.value;

    if (this.state.isFormValid && !isSignUpRequest) {
      signUp(username, password);
    } else {
      this.setState({
        data: Object.keys(this.state.data).reduce((p, c) => ({...p, [c]: {...this.state.data[c], isValidated: true} }), {})
      });
    }

    e.preventDefault();
  }

  validateField(name, value) {
    let isValid = this.state.data[name].isValid;
    let isValidated = this.state.data[name].isValidated;

    switch (name) {
      case 'username': {
        isValid = Boolean(value);
        isValidated = true;
        break;
      }
      case 'password': {
        isValid = Boolean(value);
        isValidated = true;
        break;
      }
      case 'password-confirm': {
        isValid = this.state.data.password.isValid && value === this.state.data.password.value;
        isValidated = true;
        break;
      }
      default:
        break;
    }

    this.setState({
      data: {
        ...this.state.data,
        [name]: {
          ...this.state.data[name],
          isValid,
          isValidated,
        }
      }
    }, () => {
      if (name === 'password') {
        this.validateField('password-confirm', this.state.data['password-confirm'].value);
      } else {
        this.validateForm();
      }
    });
  }

  validateForm() {
    this.setState({
      isFormValid: Object.keys(this.state.data).reduce((p, c) => p && this.state.data[c].isValid, true)
    });
  }

  getFieldValidState(name) {
    return this.state.data[name].isValidated && this.state.data[name].isValid === true ;
  }

  getFieldInvalidState(name) {
    return this.state.data[name].isValidated && this.state.data[name].isValid === false;
  }

  render() {
    const {
      isAuthenticated,
      isSignUpSuccess,
      isSignUpFailure,
      resetState,
    } = this.props;

    const {
      data,
      isFormValid,
    } = this.state;

    if (isAuthenticated) {
      resetState();
      return <Redirect to="/"/>
    }

    let result = '';
    if(isSignUpSuccess) result = (
      <UncontrolledAlert color="success">
        Успешная регистрация
      </UncontrolledAlert>
    );
    else if(isSignUpFailure) result = (
      <UncontrolledAlert color="danger">
        Произошла ошибка
      </UncontrolledAlert>
    );

    return (
      <div className="sign-up-form-container">
        <Form className="sign-up-form">
          <h4 className="text-center">Регистрация</h4>
          {result}
          <FormGroup>
            <Label for="username">Login</Label>
            <Input type="text"
                   name="username"
                   valid={this.getFieldValidState('username')}
                   invalid={this.getFieldInvalidState('username')}
                   value={data.username.value}
                   onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="password">Пароль</Label>
            <Input type="password"
                   name="password"
                   valid={this.getFieldValidState('password')}
                   invalid={this.getFieldInvalidState('password')}
                   value={data.password.value}
                   onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="password">Подтверждение пароля</Label>
            <Input type="password"
                   name="password-confirm"
                   valid={this.getFieldValidState('password-confirm')}
                   invalid={this.getFieldInvalidState('password-confirm')}
                   value={data['password-confirm'].value}
                   onChange={this.handleChange}/>
          </FormGroup>
          <Button block
                  color="primary"
                  className="mt-4"
                  disabled={!isFormValid}
                  onClick={this.handleSubmit}>Зарегистрироваться</Button>
        </Form>
      </div>
    );
  }
}

export default SignUpForm;
