import React, { Component, Fragment } from 'react';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router';
import { Link } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import {
  Collapse,
  Container,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import Catalog from '../../containers/Catalog';
import SignInPage from '../../containers/SignInPage';
import SignUpPage from '../../containers/SignUpPage';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  toggle() {
    this.setState(state => ({
      isOpen: !state.isOpen,
    }));
  }

  render() {
    const { history, store, isAuthenticated, signOut } = this.props;

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Fragment>
            <Navbar dark color="dark" expand="md">
              <Container>
                <NavbarBrand tag={Link} to="/">EveryPixel</NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse navbar isOpen={this.state.isOpen}>
                  <Nav navbar className="ml-auto">
                    {
                      !isAuthenticated
                        ? (
                          <Fragment>
                            <NavItem>
                              <NavLink tag={Link} to="/sign-in">Sign In</NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink tag={Link} to="/sign-up">Sign Up</NavLink>
                            </NavItem>
                          </Fragment>
                          )
                        : (
                          <NavItem>
                            <NavLink onClick={signOut}>Sign Out</NavLink>
                          </NavItem>
                          )
                    }
                  </Nav>
                </Collapse>
              </Container>
            </Navbar>
              <Switch>
                <Route exact path="/" component={Catalog}/>
                <Route path="/sign-in" component={SignInPage}/>
                <Route path="/sign-up" component={SignUpPage}/>
              </Switch>
          </Fragment>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
