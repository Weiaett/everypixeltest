import * as httpUtils from '../utils/httpUtils'

export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

function signInRequest() {
  return {
    type: SIGN_IN_REQUEST,
  };
}

function signInSuccess(payload) {
  return {
    type: SIGN_IN_SUCCESS,
    payload,
  };
}

function signInFailure(payload) {
  return {
    type: SIGN_IN_FAILURE,
    payload,
  };
}


export const signIn = (username, password) => {
  return dispatch => {
    dispatch(signInRequest());
    return httpUtils.signIn(username, password)
      .then(res => dispatch(signInSuccess(res)),
            err => dispatch(signInFailure(err)),);
  };
};

export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';

function signUpRequest() {
  return {
    type: SIGN_UP_REQUEST,
  };
}

function signUpSuccess(payload) {
  return {
    type: SIGN_UP_SUCCESS,
    payload,
  };
}

function signUpFailure(payload) {
  return {
    type: SIGN_UP_FAILURE,
    payload,
  };
}

export const signUp = (username, password) => {
  return dispatch => {
    dispatch(signUpRequest());
    return httpUtils.signUp(username, password)
      .then(res => {
        dispatch(signUpSuccess(res));
        return httpUtils.signIn(username, password)
        .then(res => dispatch(signInSuccess(res)),
              err => dispatch(signInFailure(err)),);
    }).catch(err => dispatch(signUpFailure(err)));
  };
};

export const SIGN_OUT = 'SIGN_OUT';

export const signOut = () => ({
  type: SIGN_OUT,
});

export const SIGN_IN_RESET_STATE = 'SIGN_IN_RESET_STATE';
export const SIGN_UP_RESET_STATE = 'SIGN_UP_RESET_STATE';

export const resetSignInState = () => ({
  type: SIGN_IN_RESET_STATE,
});

export const resetSignUpState = () => ({
  type: SIGN_UP_RESET_STATE,
});
