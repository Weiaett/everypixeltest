import * as httpUtils from '../utils/httpUtils';

export const PAGE_SIZE = 8;
export const GET_IMAGES_REQUEST = 'GET_IMAGES_REQUEST';
export const GET_IMAGES_SUCCESS = 'GET_IMAGES_SUCCESS';
export const GET_IMAGES_FAILURE = 'GET_IMAGES_FAILURE';

function getImagesRequest() {
  return {
    type: GET_IMAGES_REQUEST,
  };
}

function getImagesSuccess(payload) {
  return {
    type: GET_IMAGES_SUCCESS,
    payload,
  };
}

function getImagesFailure(payload) {
  return {
    type: GET_IMAGES_FAILURE,
    payload,
  };
}

export const getImages = (query, page) => {
  return dispatch => {
    dispatch(getImagesRequest());
    return httpUtils.getImages(query, page)
      .then(res => dispatch(getImagesSuccess({ ...res, page })),
            err => dispatch(getImagesFailure({ ...err, page })),);
  };
};

export const DELETE_IMAGE_REQUEST = 'DELETE_IMAGE_REQUEST';
export const DELETE_IMAGE_SUCCESS = 'DELETE_IMAGE_SUCCESS';
export const DELETE_IMAGE_FAILURE = 'DELETE_IMAGE_FAILURE';

function deleteImageRequest() {
  return {
    type: DELETE_IMAGE_REQUEST,
  };
}

function deleteImageSuccess(payload) {
  return {
    type: DELETE_IMAGE_SUCCESS,
    payload,
  };
}

function deleteImageFailure(payload) {
  return {
    type: DELETE_IMAGE_FAILURE,
    payload,
  };
}

export const deleteImage = (imageId, token) => {
  return dispatch => {
    dispatch(deleteImageRequest());
    return httpUtils.deleteImage(imageId, token)
      .then(res => dispatch(deleteImageSuccess({ res, imageId })),
            err => dispatch(deleteImageFailure({ err, imageId })),);
  };
};


export const UPDATE_IMAGE_REQUEST = 'UPDATE_IMAGE_REQUEST';
export const UPDATE_IMAGE_SUCCESS = 'UPDATE_IMAGE_SUCCESS';
export const UPDATE_IMAGE_FAILURE = 'UPDATE_IMAGE_FAILURE';

function updateImageRequest() {
  return {
    type: UPDATE_IMAGE_REQUEST,
  };
}

function updateImageSuccess(payload) {
  return {
    type: UPDATE_IMAGE_SUCCESS,
    payload,
  };
}

function updateImageFailure(payload) {
  return {
    type: UPDATE_IMAGE_FAILURE,
    payload,
  };
}

export const updateImage = (imageId, token, data) => {
  return dispatch => {
    dispatch(updateImageRequest());
    return httpUtils.updateImage(imageId, token, data)
      .then(res => dispatch(updateImageSuccess(res)),
            err => dispatch(updateImageFailure(err)),);
  };
};

export const CREATE_IMAGE_REQUEST = 'CREATE_IMAGE_REQUEST';
export const CREATE_IMAGE_SUCCESS = 'CREATE_IMAGE_SUCCESS';
export const CREATE_IMAGE_FAILURE = 'CREATE_IMAGE_FAILURE';

function createImageRequest() {
  return {
    type: CREATE_IMAGE_REQUEST,
  };
}

function createImageSuccess(payload) {
  return {
    type: CREATE_IMAGE_SUCCESS,
    payload,
  };
}

function createImageFailure(payload) {
  return {
    type: CREATE_IMAGE_FAILURE,
    payload,
  };
}

export const createImage = (token, data) => {
  return dispatch => {
    dispatch(createImageRequest());
    return httpUtils.createImage(token, data)
      .then(res => dispatch(createImageSuccess(res)),
            err => dispatch(createImageFailure(err)),);
  };
};
