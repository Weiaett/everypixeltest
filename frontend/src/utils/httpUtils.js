import { PAGE_SIZE } from '../actions/catalog';

const HOST = 'http://127.0.0.1:8000';

export const signIn = (username, password) => {
  return fetch(HOST + '/api/users/auth/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  }).then(res => {
    const json = res.json();
    if (res.status >= 400) throw json; else return json;
  });
};

export const signUp = (username, password) => {
  return fetch(HOST + '/api/users/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  }).then(res => {
    const json = res.json();
    if (res.status >= 400) throw json; else return json;
  });
};

export const getImages = (query, page) => {
  query = Object.entries(query).map(([k, v]) => encodeURIComponent(k) + '=' + encodeURIComponent(v)).join('&');
  query += `&page=${page}&pagesize=${PAGE_SIZE}`;
  return fetch(HOST + `/api/images/?${query}`).then(res => {
    const json = res.json();
    if (res.status >= 400) throw json; else return json;
  });
};

export const deleteImage = (imageId, token) => {
  return fetch(HOST + '/api/images/' + imageId + '/', {
    method: 'DELETE',
    headers: {
      'Authorization': 'Bearer ' + token,
    },
  }).then(res => {
    if (res.status >= 400) throw Object.create({}); else return {};
  });
};


export const updateImage = (imageId, token, data) => {
  return fetch(HOST + '/api/images/' + imageId + '/', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
    },
    body: JSON.stringify({
      ...data
    }),
  }).then(res => {
    const json = res.json();
    if (res.status >= 400) throw json; else return json;
  });
};

export const createImage = (token, data) => {
  const formData = new FormData();
  for (const key in data) {
    formData.set(key, data[key]);
  }

  return fetch(HOST + '/api/images/', {
    method: 'POST',
    headers: {
      'Authorization': 'Bearer ' + token,
    },
    body: formData,
  }).then(res => {
    const json = res.json();
    if (res.status >= 400) throw json; else return json;
  });
};
