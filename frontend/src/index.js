import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import './index.css';
import fontawesome from '@fortawesome/fontawesome';
import fontawesomeRegular from '@fortawesome/fontawesome-free-regular';
import fontawesomeSolid from '@fortawesome/fontawesome-free-solid';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import { history, store } from './store';

fontawesome.library.add(fontawesomeRegular.faEdit, fontawesomeSolid.faPlus);

ReactDOM.render(<App history={history} store={store}/>, document.getElementById('root'));
registerServiceWorker();
