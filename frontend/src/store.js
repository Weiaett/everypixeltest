import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from './reducers/index';

export const history = createHistory();
export const store = createStore(
  rootReducer,
  {},
  compose(
    applyMiddleware(
      routerMiddleware(history),
      thunk,
    ),
  ),
);
