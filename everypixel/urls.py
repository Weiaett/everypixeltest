from django.conf.urls import include
from django.conf.urls import static
from django.conf.urls import url
from django.contrib import admin

import everypixel.settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('photobank.urls')),
]

urlpatterns += static.static(everypixel.settings.MEDIA_URL, document_root=everypixel.settings.MEDIA_ROOT)
